package de.favo.ttstool;

import android.annotation.TargetApi;
import android.os.Build;
import android.service.quicksettings.Tile;
import android.service.quicksettings.TileService;
import android.speech.tts.TextToSpeech;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/*
        TTS Tool - A simple GUI for android's Text-To-Speech-Engines
        Copyright (C) 2018  Felix v. Oertzen
        felix@von-oertzen-berlin.de

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

@TargetApi(Build.VERSION_CODES.N)
public class SayTimeService extends TileService {

    private TextToSpeech    mTextToSpeechEngine;
    private Locale          mDefaultLocale;
    private Calendar        mCalendar;

    public SayTimeService(){
        super();
        mDefaultLocale = Locale.getDefault();
        mDefaultLocale = (mDefaultLocale.getDisplayLanguage().equals(Locale.GERMAN.getDisplayLanguage())) ?
                Locale.GERMANY : Locale.UK;
    }

    @Override
    public void onTileAdded(){
        Tile t = getQsTile();
        t.setState(Tile.STATE_UNAVAILABLE);
        t.updateTile();
    }

    @Override
    public void onTileRemoved(){
        //nothing
    }

    @Override
    public void onStartListening() {
        super.onStartListening();

        mTextToSpeechEngine = initiateTextToSpeechEngine(mDefaultLocale);
        mCalendar = GregorianCalendar.getInstance();
        Tile t = getQsTile();
        t.setLabel(getString(R.string.text_say_time));
        t.setState(Tile.STATE_UNAVAILABLE);
        t.updateTile();
    }

    @Override
    public void onStopListening() {
        super.onStopListening();

        Tile t = getQsTile();
        t.setState(Tile.STATE_UNAVAILABLE);
        t.updateTile();

        mTextToSpeechEngine.stop();
        mTextToSpeechEngine.shutdown();
        mTextToSpeechEngine = null;
    }

    private int hour(int add){
        int ret = (((mCalendar.get(Calendar.AM_PM) == Calendar.PM ) &&
                (mCalendar.get(Calendar.HOUR) == 0)) ? 12 : mCalendar.get(Calendar.HOUR)) + add;
        return ret > 12 ? ret - 12 : ret;
    }

    @Override
    public void onClick() {
        super.onClick();

        String mTime = "";
        mCalendar.setTime(new Date(System.currentTimeMillis()));

        if (mDefaultLocale.getDisplayLanguage().equals(Locale.GERMAN.getDisplayLanguage())) {

            if (mCalendar.get(Calendar.MINUTE) == 0)
                mTime = getString(R.string.time_hh, getGermanHour(0,false));
            else if (mCalendar.get(Calendar.MINUTE) == 15)
                mTime = getString(R.string.time_quarter_past, getGermanHour(0,true));
            else if (mCalendar.get(Calendar.MINUTE) == 45)
                mTime = getString(R.string.time_quarter_to, getGermanHour(1,true));
            else if (mCalendar.get(Calendar.MINUTE) == 30)
                mTime = getString(R.string.time_half, getGermanHour(1,false));
            else if (mCalendar.get(Calendar.MINUTE) % 10 == 0){
                    mTime = getString(R.string.time_its_past, getGermanHour(0,false),""+ mCalendar.get(Calendar.MINUTE));
            }else if (mCalendar.get(Calendar.MINUTE) % 10 != 0){
                if (mCalendar.get(Calendar.MINUTE) < 30)
                    mTime = getString(
                            mCalendar.get(Calendar.MINUTE) == 1 ?
                                    R.string.time_its_past_minute : R.string.time_its_past_minutes,
                            mCalendar.get(Calendar.MINUTE) == 1 ?
                                        "eine" : mCalendar.get(Calendar.MINUTE)+"",
                            getGermanHour(0,false));
                else
                    mTime = getString(mCalendar.get(Calendar.MINUTE) == 59 ?
                            R.string.time_its_to_minute : R.string.time_its_to_minutes,
                            mCalendar.get(Calendar.MINUTE) == 59 ?
                                    "eine" : 60- mCalendar.get(Calendar.MINUTE),
                            getGermanHour(1,false));
            }
        } else {
            if (0 == mCalendar.get(Calendar.MINUTE))
                mTime = (getString(R.string.time_hh, ""+hour(0)));
            else if (mCalendar.get(Calendar.MINUTE)== 15)
                mTime = (getString(R.string.time_quarter_past, ""+hour(0)));
            else if (mCalendar.get(Calendar.MINUTE) == 30)
                mTime = (getString(R.string.time_half, ""+hour(0)));
            else if (mCalendar.get(Calendar.MINUTE) == 45)
                mTime = (getString(R.string.time_quarter_to, ""+hour(1)));
            else if (mCalendar.get(Calendar.MINUTE) % 5 != 0){
                if (mCalendar.get(Calendar.MINUTE) < 30)
                    mTime = (getString(
                            mCalendar.get(Calendar.MINUTE)+1-1 != 1 ? R.string.time_its_past_minutes:R.string.time_its_past_minute,
                            mCalendar.get(Calendar.MINUTE),hour(0)));
                else
                    mTime = (getString(
                            ((mCalendar.get(Calendar.MINUTE)+1-1 != 1) && (mCalendar.get(Calendar.MINUTE)+1-1 != 59)) ? R.string.time_its_to_minutes : R.string.time_its_to_minute,
                            (60- mCalendar.get(Calendar.MINUTE)),hour(1)));

            } else if (mCalendar.get(Calendar.MINUTE) % 5 == 0){
                if (mCalendar.get(Calendar.MINUTE) < 30)
                    mTime = (getString(R.string.time_its_past,""+ mCalendar.get(Calendar.MINUTE),""+hour(0)));
                else
                    mTime = (getString(R.string.time_its_to,(""+(60 - mCalendar.get(Calendar.MINUTE))),""+hour(1)));

            }
        }
        if (mCalendar.get(Calendar.HOUR_OF_DAY)+1-1 == 12)
            mTime = mTime + " " + getString(R.string.in_the_noon);
        else
            mTime = mTime + " " +
                    (mCalendar.get(Calendar.AM_PM)+1-1
                            == Calendar.AM ? getString(R.string.in_the_morning) :
                            ((hour(0)+1-1 >= 6))? getString(R.string.in_the_evening) : getString(R.string.in_the_afternoon));
        talk(mTime);
    }

    private String getGermanHour(int p, boolean s){
        if (hour(p) == 1 && !s)
                return "ein";
        else
            return hour(p) + "";
    }

    private void talk(String toSpeak){
        toSpeak = checkIfTextIsPronounceable(toSpeak);
        mTextToSpeechEngine.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null, null);

    }
    private String checkIfTextIsPronounceable(String text){
        return text.length() >= TextToSpeech.getMaxSpeechInputLength() ?
                getString(R.string.warning_too_many_chars) : text;
    }

    private void noEngineFound(){
        Toast.makeText(this, R.string.error_no_tts,
                Toast.LENGTH_SHORT).show();
    }

    private TextToSpeech initiateTextToSpeechEngine(final Locale mLocale){
        TextToSpeech.OnInitListener mTextToSpeechOnInitListener = new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status == TextToSpeech.SUCCESS) {
                    mTextToSpeechEngine.setLanguage(mLocale);
                    Tile t = getQsTile();
                    t.setState(Tile.STATE_ACTIVE);
                    t.updateTile();
                }
                if (status == TextToSpeech.ERROR_NOT_INSTALLED_YET){
                    noEngineFound();
                }
                if ((status == TextToSpeech.ERROR_NETWORK)||(status == TextToSpeech.ERROR_NETWORK_TIMEOUT)){
                    Toast.makeText(SayTimeService.this, R.string.error_network, Toast.LENGTH_SHORT).show();
                }
                if (status == TextToSpeech.ERROR_OUTPUT){
                    Toast.makeText(SayTimeService.this, R.string.error_i_o, Toast.LENGTH_SHORT).show();

                }
                if ((status == TextToSpeech.ERROR_SYNTHESIS)||(status == TextToSpeech.ERROR_SERVICE)){
                    Toast.makeText(SayTimeService.this, R.string.error_general_tts, Toast.LENGTH_SHORT).show();
                }
            }
        };
        return new TextToSpeech(getApplicationContext(),mTextToSpeechOnInitListener);
    }

}
