package de.favo.ttstool;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

/*
        TTS Tool - A simple GUI for android's Text-To-Speech-Engines
        Copyright (C) 2018  Felix v. Oertzen
        felix@von-oertzen-berlin.de

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

public class MainActivity extends AppCompatActivity {
    static          final String SHARED_PREFS       = "TTS-PREF";
    private static  final String SHARED_PREFS_PITCH = "TTS-PREf_PITCH";
    private static  final String TTS_PACKAGE        = "TTS-PACKAGE";
    
    private TextToSpeech    mTextToSpeechEngine;
    private Button          mSpeakButton;
    private Button          mStopButton;
    private EditText        mTextEdit;
    private Spinner         mLocaleSpinner;
    private Spinner         mSpeedSpinner;
    private Spinner         mEngineSpinner;
    private boolean         mFirstResumeBoolean = true;
    private boolean         mFirstStartBoolean  = true;
    private boolean         mSpeakNowBoolean    = false;
    
    private ProgressDialog  mInitiateProgressDialog;
    private Locale          defaultLocale = Locale.getDefault();
    private String          mTtsPackage;
    private SeekBar         mPitchSeekBar;
    
    private SharedPreferences           mSharedPreferences;
    private SharedPreferences.Editor    mSharedPreferencesEditor;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        mSharedPreferences = this.getSharedPreferences(SHARED_PREFS,MODE_PRIVATE);
        mSharedPreferencesEditor = mSharedPreferences.edit();
        
        setContentView(R.layout.activity_main);
        
        mTextEdit       = findViewById(R.id.text);
        mSpeakButton    = findViewById(R.id.speak);
        mStopButton     = findViewById(R.id.stop);
        mLocaleSpinner  = findViewById(R.id.spinner);
        mSpeedSpinner   = findViewById(R.id.spinner2);
        mEngineSpinner  = findViewById(R.id.spinner3);
        mPitchSeekBar   = findViewById(R.id.seekBar);
        
        mSpeakButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mTextToSpeakString = mTextEdit.getText().toString();
                talk(mTextToSpeakString);

            }
        });
        
        mPitchSeekBar.setMax(20);
        mPitchSeekBar.setProgress(mSharedPreferences.getInt(SHARED_PREFS_PITCH,10));
        
        mPitchSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mInitiateProgressDialog.isShowing())
                    return;
                if (progress == 0)
                    progress = 1;
                mSharedPreferencesEditor.putInt(SHARED_PREFS_PITCH,progress);
                mSharedPreferencesEditor.apply();
                
                if (mTextToSpeechEngine != null)
                    mTextToSpeechEngine.setPitch(((float)progress)/((float)10));
            }
            
            public void onStartTrackingTouch(SeekBar seekBar) {}
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
        
        mStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTextToSpeechEngine.stop();
            }
        });
        
        
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        mTtsPackage = intent.getStringExtra(TTS_PACKAGE);

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("mTextEdit/plain".equals(type)) {
                String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
                if (sharedText != null) {
                    mTextEdit.setText(sharedText);
                    mSpeakNowBoolean = true;
                }}
        }

        initiateApp(mTtsPackage);

    }
    private void initiateApp(String package0){
        mInitiateProgressDialog = ProgressDialog.show(this,getString(R.string.text_please_wait) ,
                getString(R.string.text_initiating_tts), true);

        SharedPreferences sp = this.getSharedPreferences(SHARED_PREFS,MODE_PRIVATE);
        String tmp_lang = sp.getString("lang",Locale.getDefault().getDisplayName());
        Locale langs[] = Locale.getAvailableLocales();
        for (Locale lang : langs) {
            if (lang.getDisplayName().equals(tmp_lang))
                defaultLocale = lang;
        }

        mTextToSpeechEngine = initiateTextToSpeechEngine(defaultLocale,package0);
        if ((mTextToSpeechEngine.getEngines().size() == 0)){
            noEngine();
        }
    }
    
    private void noEngine(){
        new AlertDialog.Builder(MainActivity.this)
                .setTitle(R.string.error_general_tts)
                .setMessage(R.string.error_no_tts)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.this.finish();
                    }
                })
                .setNeutralButton(R.string.text_install_tts_engine, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String appPackageName ="com.google.android.mTextToSpeechEngine";
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                })
                .show();
    }
    
    private void talk(String mTextToSpeak){
        mTextToSpeak = checkIfTextIsPronounceable(mTextToSpeak);
        
        Toast.makeText(getApplicationContext(), mTextToSpeak,Toast.LENGTH_SHORT).show();
        
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mTextToSpeechEngine.speak(mTextToSpeak, TextToSpeech.QUEUE_FLUSH, null, null);
        }else{
            mTextToSpeechEngine.speak(mTextToSpeak, TextToSpeech.QUEUE_FLUSH, null);
        }
    }
    
    private String checkIfTextIsPronounceable(String text){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
            if (text.length() >= TextToSpeech.getMaxSpeechInputLength())
                text = getString(R.string.warning_too_many_chars);
        return text;
    }

    private void shareText(String mTextToSpeak) throws IOException {
        Random mRandom = new Random();
        final ProgressDialog dialog = ProgressDialog
                .show(this,"", getString(R.string.text_please_wait),
                        true, false);
        final Uri mContentUri;

        File mCacheRootDirectory = getCacheDir();
        int random = mRandom.nextInt(800 - 1) + 65;

        final File mTemporaryOutputFile = new File(mCacheRootDirectory +
                "/share/SharedAudio" + random + ".wav");
        final File mShareableDirectory = new File (mCacheRootDirectory + "/share/");

        mTextToSpeak = checkIfTextIsPronounceable(mTextToSpeak + "!!    .  ."/*It's neccessary to avoid missing characters in whatsapp*/);
        mShareableDirectory.mkdir();
        mTemporaryOutputFile.createNewFile();

        mContentUri = FileProvider.getUriForFile(this,
                "de.favo.ttstool.fileProvider", mTemporaryOutputFile);

        mTextToSpeechEngine.setOnUtteranceProgressListener(new UtteranceProgressListener() {

            @Override
            public void onStart(String utteranceId) {

            }

            @Override
            public void onDone(String utteranceId) {
                dialog.dismiss();
                    Intent mShareIntent = new Intent();
                    mShareIntent.setAction(Intent.ACTION_SEND);
                    mShareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    mShareIntent.setDataAndType(mContentUri, "audio/mp3");
                    mShareIntent.putExtra(Intent.EXTRA_STREAM, mContentUri);
                    startActivity(Intent.createChooser(mShareIntent, getString(R.string.hint_choose_app)));
            }

            @Override
            public void onError(String utteranceId) {
                dialog.dismiss();
            }

        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            mTextToSpeechEngine.synthesizeToFile(mTextToSpeak, null,
                    (mTemporaryOutputFile),random + "");
        else
            mTextToSpeechEngine.synthesizeToFile(mTextToSpeak, null,
                    mTemporaryOutputFile.toString());
    }

    private void initiateTTSEngine(){
        { // Languages
            final List<String> mActionsList = new ArrayList<>();
            final List<Locale> mAvailableLanguagesList = getAvailableTextToSpeechLanguages(mTextToSpeechEngine);
            ArrayAdapter<String> mDataAdapter = new ArrayAdapter<>(this,
                    android.R.layout.simple_spinner_item, mActionsList);

            for (int i = 0; i < mAvailableLanguagesList.size(); i++)
                mActionsList.add(((Locale) mAvailableLanguagesList.toArray()[i]).getDisplayName());

            mDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            String compareValue = defaultLocale.getDisplayName();
            mLocaleSpinner.setAdapter(mDataAdapter);

            if (compareValue != null)
                mLocaleSpinner.setSelection(mDataAdapter.getPosition(compareValue));

            mLocaleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    for (int i = 0; i < mAvailableLanguagesList.size(); i++) {
                        String loc = mActionsList.get(position);
                        if (loc.equals(mAvailableLanguagesList.get(i).getDisplayName())) {
                            mTextToSpeechEngine.setLanguage(mAvailableLanguagesList.get(i));
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

        }

        { // text-to-speech-engines
            final List<String> mActionsList = new ArrayList<>();
            final List<TextToSpeech.EngineInfo> mEnginesList = mTextToSpeechEngine.getEngines();
            ArrayAdapter<String> mDataAdapter = new ArrayAdapter<>(this,
                    android.R.layout.simple_spinner_item, mActionsList);
            for (int i = 0; i < mEnginesList.size(); i++)
                mActionsList.add(mEnginesList.get(i).label);

            mDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mEngineSpinner.setAdapter(mDataAdapter);

            for (int i = 0; i< mEnginesList.size();i++)
                if (mTtsPackage != null)
                    if (mTtsPackage.equals(mEnginesList.get(i).name))
                        mEngineSpinner.setSelection(i);

            mEngineSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    for (int i = 0; i < mEnginesList.size(); i++) {
                        String eng = mActionsList.get(position);
                        if (eng.equals(mEnginesList.get(i).label)) {
                            if (!mFirstStartBoolean)
                                startActivity(new Intent(MainActivity.this,MainActivity.class).putExtra(TTS_PACKAGE,mEnginesList.get(i).name));
                            mFirstStartBoolean = false;
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }

        { // speed settings
            List<String> mSpeedList = new ArrayList<>();
            ArrayAdapter<String> mDataAdapter = new ArrayAdapter<>(this,
                    android.R.layout.simple_spinner_item, mSpeedList);
                mSpeedList.add(getString(R.string.speed_very_slow));
                mSpeedList.add(getString(R.string.speed_slow));
                mSpeedList.add(getString(R.string.speed_normal));
                mSpeedList.add(getString(R.string.speed_fast));
                mSpeedList.add(getString(R.string.speed_very_fast));

            mDataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            String compareValue = getString(R.string.speed_normal);
            mSpeedSpinner.setAdapter(mDataAdapter);
            mSpeedSpinner.setSelection(mDataAdapter.getPosition(compareValue));
            mSpeedSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    float speed = Float.parseFloat("1.0");
                    if (position == 0){speed = Float.parseFloat("0.25");}
                    if (position == 1){speed = Float.parseFloat("0.50");}
                    if (position == 2){speed = Float.parseFloat("1.0");}
                    if (position == 3){speed = Float.parseFloat("2.0");}
                    if (position == 4){speed = Float.parseFloat("3.0");}


                    mTextToSpeechEngine.setSpeechRate(speed);
                }

                @Override public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            mInitiateProgressDialog.dismiss();
            if(mSpeakNowBoolean)
                mSpeakButton.callOnClick();
        }
    }

    public void onPause(){
        if(mTextToSpeechEngine != null){
            mTextToSpeechEngine.stop();
            mTextToSpeechEngine.shutdown();
        }

        if (!mInitiateProgressDialog.isShowing() && mLocaleSpinner != null && mLocaleSpinner.getSelectedItem() != null)
            mSharedPreferencesEditor.putString("lang",mLocaleSpinner.getSelectedItem().toString());
        else{
            try {
                finish();
            }catch (Exception ignored){}
        }

        mSharedPreferencesEditor.apply();
        super.onPause();
    }

    public void onResume(){
        super.onResume();
        if (!mFirstResumeBoolean)
            initiateApp(mTtsPackage);
        mFirstResumeBoolean = false;
        mFirstStartBoolean = true;
    }

    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }

    private TextToSpeech initiateTextToSpeechEngine(final Locale mLocale, String enginePackage){
        TextToSpeech.OnInitListener mTextToSpeechOnInitListener = new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status == TextToSpeech.SUCCESS) {
                    initiateTTSEngine();
                    mTextToSpeechEngine.setLanguage(mLocale);
                }
                if (status == TextToSpeech.ERROR_NOT_INSTALLED_YET)
                    noEngine();
                if ((status == TextToSpeech.ERROR_NETWORK)||(status == TextToSpeech.ERROR_NETWORK_TIMEOUT))
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle(R.string.error_general_tts)
                            .setMessage(R.string.error_network)
                            .setPositiveButton("OK",null)
                            .show();
                if (status == TextToSpeech.ERROR_OUTPUT)
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle(R.string.error_general_tts)
                            .setMessage(R.string.error_i_o)
                            .setPositiveButton("OK",null)
                            .show();
                if ((status == TextToSpeech.ERROR_SYNTHESIS)||(status == TextToSpeech.ERROR_SERVICE))
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle(R.string.error_general_tts)
                            .setMessage(R.string.error_general_tts)
                            .setPositiveButton("OK",null)
                            .show();
            }
        };

        if (enginePackage == null)
            return new TextToSpeech(getApplicationContext(),mTextToSpeechOnInitListener);
        else
            return new TextToSpeech(getApplicationContext(),mTextToSpeechOnInitListener,enginePackage);
    }
    public List<Locale> getAvailableTextToSpeechLanguages(TextToSpeech tts){
        List<Locale> localeList = new ArrayList<>();
        Locale[] locales;
        try {
            locales = Locale.getAvailableLocales();
            for (Locale locale : locales) {
                int res = tts.isLanguageAvailable(locale);
                if (res == TextToSpeech.LANG_COUNTRY_AVAILABLE) {
                    localeList.add(locale);
                }

            }
        }catch (Exception ignored){}
        return localeList;
    }
    private void saveAndShare(){
        try {
            shareText(mTextEdit.getText().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_share: {
                if (!mTextEdit.getText().toString().isEmpty())
                    saveAndShare();
                else
                    Toast.makeText(MainActivity.this, R.string.error_empty, Toast.LENGTH_SHORT).show();
                return true;
            }
            case R.id.menu_web: {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.url_web))));
                break;
            }
            case R.id.menu_shop: {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/dev?id=4748585467657800463")));
                break;
            }
            case R.id.menu_legal: {
                startActivity(new Intent(getApplicationContext(), StartActivity.class));
                break;
            }
            case R.id.menu_about: {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.url_me))));
                break;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
        // Handle your other action bar items...
        return super.onOptionsItemSelected(item);
    }
}
